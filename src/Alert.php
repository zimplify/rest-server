<?php
    namespace Zimplify\Communication;
    use Zimplify\Core\Reply;
    use \Exception;

    /**
     * Alert is the instance to expose errors to responses
     * @package Zimplify\Communication (code 04)
     * @type instance (code 1)
     * @file Alert (code 01)
     */
    final class Alert extends Reply {

        /**
         * define the error into the reply.
         * @param Exception $ex the exception to add
         * @return Alert
         */
        public function withError() {
            $c = $this->data->getCode();
            $m = $this->data->getMessage(); 
            $fc = substr((string) $c, 0, 3);
            $fc = (int) $fc == 0 ? 500 : $fc;
            return $this->withJson(["message" => $m, "code" => $c])->withStatus($fc);
        }

    }