<?php
    namespace Zimplify\Communication;
    use Slim\Psr7\Response;

    /**
     * reply is the object we offer to the controller to send and manage the response process
     * @package Zimplify\Communication (code 04)
     * @type instance (code 1)
     * @file Reply (code 01)
     */
    class Reply {

        protected $code = 200;
        protected $type = null;
        protected $headers = [];
        protected $isJson = true;
        protected $data = null;

        /**
         * constructor: creating a new instance
         * @param mixed $data the data to add
         * @return void
         */
        function __construct($data = null) {
            $this->data = $data;       
        }

        /**
         * set the data of the response
         * @param string $type the MIME data type
         * @param mixed $data the data to set into the instnace
         * @return Reply 
         */
        function withData(string $type, $data) : self {
            $this->type = $type;
            $this->data - $data;
            return $this;
        }

        /**
         * set the data to the response as JSON data
         * @param mixed $data the variable to return
         * @return Reply
         */
        function withJson($data) : self {
            $this->data = $data;
            $this->type = "application/json";
            $this->isJson = true;
            return $this;
        }

        /**
         * set the header of the response
         * @param Array $headers the header(s) to add onto the response
         * @return Reply
         */
        function withHeader(Array $headers) : self {
            foreach($headers as $f => $v) 
                if (!array_key_exists($f, $this->headers)) $this->headers[$f] = $v;
            return $this;
        }

        /**
         * set the status code of the response
         * @param int $code the status code to set
         * @return Reply
         */
        function withStatus(int $code) : self {
            $this->code = $code;
            return $this;
        }

        /**
         * output our reply as a HTTP response and send back
         * @return Response
         */
        function flush() : Response {
            $result = new Response();
            $result = $result->withStatus($this->code);

            // transferring the headers
            foreach ($this->headers as $f => $v) 
                $result = $result->withHeader($f, $v);

            // mark if the object is there, we need to write the JSON data
            $result = $result->withHeader('Content-Type', $this->type);
            
            // now write the data
            $result->write($this->isJson ? json_encode($this->data) : $this->data);

            // the result to send back
            return $result;
        }
    }